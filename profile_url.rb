# coding: utf-8
#
# DO NOT UPDATE THIS FILE; see Gitlab::Profiler in the main Rails app
# instead. This file is for instances pre-10.5, that don't have that module
# available.
#
# Profiles a URL on an omnibus GitLab installation. To run, run as root:
#
# sudo gitlab-rails runner /tmp/profile_url.rb <URL to profile> -u username -o <ruby-prof HTML filename> -s <SQL output filename>
#
# For example:
#
# sudo gitlab-rails runner /tmp/profile_url.rb "https://gitlab.com/gitlab-org/gitlab-ce/issues" -u username -o /tmp/profile.html -s /tmp/profile_sql.txt
#
require 'ruby-prof'
require 'optparse'

options = {}

opt_parser = OptionParser.new do |opt|
  opt.banner = 'Usage: profile_url.rb [options]'
  opt.separator ''
  opt.separator 'Example:'
  opt.separator ''
  opt.separator '     profile_url.rb https://gitlab.com/gitlab-org/gitlab-ce/issues -u root -o /tmp/profile.html -s /tmp/profile_sql.txt'
  opt.separator ''
  opt.separator 'Options:'

  opt.on('-u', '--user=root', 'User to authenticate as') do |username|
    options[:username] = username
  end

  opt.on('-o', '--output /tmp/profile.html', 'profile output filename') do |output|
    options[:profile_output] = output
  end

  opt.on('-s', '--sql /tmp/profile_sql.txt', 'SQL output filename') do |sql|
    options[:sql_output] = sql
  end

  opt.on('-p', "--post {:user => 'john', :pass => 'test'}", 'Send HTTP POST data') do |post_data|
    options[:post_data] = post_data
  end
end

opt_parser.parse!
options[:url] = ARGV[0]

if options[:url].nil? ||
   options[:profile_output].nil? ||
   options[:sql_output].nil?
  puts opt_parser
  exit
end

username = 'anonymous'

if options[:username]
  user = UserFinder.new(options[:username]).find_by_username

  raise "User #{options[:username]} not found" unless user

  username = user.username
  API::Helpers::CommonHelpers.send(:define_method, :find_current_user!) { user }
  ApplicationController.send(:define_method, :current_user) { user }
  ApplicationController.send(:define_method, :authenticate_user!) { }
end

IGNORE_BACKTRACES = [
  'lib/gitlab/i18n.rb',
  'lib/gitlab/request_context.rb',
  'config/initializers',
  'lib/gitlab/database/load_balancing/',
  'lib/gitlab/etag_caching/',
  'lib/gitlab/metrics/',
  'lib/gitlab/middleware/',
  'lib/gitlab/performance_bar/',
  'lib/gitlab/request_profiler/'
]

url = options[:url]
$load_times_by_model = {}

ActiveSupport::LogSubscriber.colorize_logging = false
logger = Logger.new(options[:sql_output])

class << logger
  def debug(message, *)
    _, type, time = *message.match(/(\w+) Load \(([0-9.]+)ms\)/)

    if type && time
      $load_times_by_model[type] ||= 0
      $load_times_by_model[type] += time.to_f
    end

    super

    backtrace = Rails.backtrace_cleaner.clean caller

    backtrace.each do |caller_line|
      next if caller_line.match(Regexp.union(IGNORE_BACKTRACES))

      super("  ↳ #{caller_line.sub("#{Rails.root}/", '')}")
    end
  end
end

ActiveRecord::Base.logger = logger
ActionController::Base.logger = logger

app = ActionDispatch::Integration::Session.new(Rails.application)
puts "Loading #{url} as #{username}"

headers = {}

# This allows us to get API timings in the logs
def load_grape_logging(logger)
  API::API.use GrapeLogging::Middleware::RequestLogger,
               # Create a separate logger to avoid intercepting other log messages
               logger: logger,
               formatter: Gitlab::GrapeLogging::Formatters::LogrageWithTimestamp.new,
               include: [
                 GrapeLogging::Loggers::FilterParameters.new,
                 GrapeLogging::Loggers::ClientEnv.new,
                 Gitlab::GrapeLogging::Loggers::UserLogger.new
               ]
end

def api_request?(url)
  url.match(/api\/v[34]/)
end

def get_token_header(url)
  if api_request?(url)
    'HTTP_PRIVATE_TOKEN'
  else
    'PRIVATE-TOKEN'
  end
end

if ENV['PRIVATE_TOKEN']
  token_header = get_token_header(url)
  headers = { token_header => ENV['PRIVATE_TOKEN'] }
  puts "Using #{token_header} to load URL"
end

# API requests can sometime take a while to mount Grape endpoints. Load
# this URL without profiling to take that out of the equation.
load_grape_logging(Logger.new(options[:sql_output]))
app.get('/api/v4/users')

# Rails loads internationalization files lazily the first time a translation is
# needed. Running this prevents this overhead from showing up in profiles.
I18n.t(".")[:test_string]

RequestStore.begin!
RubyProf.start

if options[:post_data]
  headers['Content-Type'] = 'application/json'
  app.post(url, params: options[:post_data], headers: headers)
else
  app.get(url, params: nil, headers: headers)
end

$load_times_by_model.to_a.sort_by(&:last).reverse.each do |(model, time)|
  logger.info("#{model} total: #{time.round(2)}ms")
end

result = RubyProf.stop
printer = RubyProf::CallStackPrinter.new(result)
file = File.open(options[:profile_output], 'w')
printer.print(file)
file.close
RequestStore.end!
