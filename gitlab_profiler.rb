#!/bin/env ruby

require 'json'
require 'securerandom'
require 'uri'
require 'sequel'
require 'English'
require 'yaml'

# Updates the DB with results of the profile
class DatabaseUpdater
  attr_reader :database_url, :gitlab_version
  attr_accessor :db

  def initialize(url, gitlab_version)
    @database_url = url
    @gitlab_version = gitlab_version
  end

  def update_db(data)
    prepare_db

    stats = db[:request_stats]

    stats.insert(test_group: data[:test_group],
                 url: data[:url],
                 auth_type: data[:auth_type],
                 html_url: data[:html_url],
                 sql_url: data[:sql_url],
                 status_code: data[:status_code],
                 status_description: data[:status_description],
                 view_time_ms: data[:view_time_ms],
                 sql_time_ms: data[:sql_time_ms],
                 total_ms: data[:total_ms],
                 gitlab_version: gitlab_version,
                 created_at: Time.now,
                 started_at: data[:started_at],
                 ended_at: data[:ended_at])
    disconnect_db
  end

  def find_urls_for_version(version)
    prepare_db

    stats = db[:request_stats]

    stats.select(:auth_type, :url).where(gitlab_version: version)
  end

  private

  def prepare_db
    refresh_connection
    create_db_table
  end

  def refresh_connection
    @db = Sequel.connect(database_url)
  end

  def disconnect_db
    @db.disconnect
    @db = nil
  end

  def create_db_table
    return if db.table_exists?(:request_stats)

    db.create_table :request_stats do
      primary_key :id
      String :test_group
      String :url
      String :auth_type
      String :html_url
      String :sql_url
      Integer :status_code
      String :status_description
      Float :view_time_ms
      Float :sql_time_ms
      Float :total_ms
      Timestamp :created_at
      Timestamp :started_at
      Timestamp :ended_at
      String :gitlab_version
    end
  end
end

# This class does a number of tasks:
# 1. Profiles GitLab URLs within a worker machine
# 2. Records ruby-prof data and SQL debug dumps
# 3. Saves the files to S3 (awscli is expected to be installed)
# 4. Updates a database
class GitLabProfiler
  attr_reader :gitlab_version
  attr_reader :static_domain, :bucket_name, :database_url, :profiler_config
  attr_reader :auth_config
  attr_reader :base_directory, :db_updater

  REDACTED_STR = '**REDACTED**'.freeze
  BUNDLED_PROFILE_SCRIPT = '/opt/gitlab/embedded/service/gitlab-rails/bin/profile-url'.freeze
  DEPRECATED_PROFILE_SCRIPT = File.expand_path('./profile_url.rb').freeze

  def initialize(config_file)
    load_config_yaml(config_file)
    @base_directory = today_date
    @gitlab_version = load_gitlab_version
    @db_updater = DatabaseUpdater.new(database_url, gitlab_version)
  end

  def use_bundled_script?
    return @use_bundled_script if defined?(@use_bundled_script)

    @use_bundled_script = File.exist?(BUNDLED_PROFILE_SCRIPT)
  end

  # Unless you have write access to the directory where the profiler script
  # lives, Rails won't attempt to run the script. We copy it to /tmp to
  # workaround this problem.
  def bundled_script
    return BUNDLED_PROFILE_SCRIPT if File.writable?(File.dirname(BUNDLED_PROFILE_SCRIPT))

    temp_script = File.join('/tmp', File.basename(BUNDLED_PROFILE_SCRIPT))
    puts "Copying profile script to /tmp"
    system("cp #{BUNDLED_PROFILE_SCRIPT} #{temp_script}")
    # We don't need require statements, since one of them expects
    # to load the environment from the path of the script
    system("sed -e 's/require .*//g' -i #{temp_script}")
    temp_script
  end

  def profile
    perf_data = []

    entries_to_profile.each do |entry|
      url = entry['url']
      auth_types = entry.fetch('auth', ['anonymous'])

      next unless url

      auth_types.each do |auth|
        json_data = run_profile(url, auth)
        perf_data << json_data if json_data

        next if json_data[:status_code] == 500

        upload_files(json_data)
        update_db(json_data)
      end
    end

    perf_data
  end

  private

  def entries_to_profile(load_missing: true)
    return profiler_config unless load_missing

    processed_urls = db_updater.find_urls_for_version(gitlab_version)

    puts "Found #{processed_urls.count} already processed URLs"

    entries = profiler_config.select do |config|
      ok = true
      processed_urls.each do |row|
        if row[:url] == config['url'] &&
           row.fetch(:auth_type, 'anonymous') == config.fetch('auth', ['anonymous']).join
           puts "Discarding #{row} since this has already been processed"
	       ok = false
           break
        end
      end

      ok
    end

    puts "Processing #{entries.count} URLs (#{profiler_config.count - entries.count} removed)"

    entries
  end

  def load_gitlab_version
    begin
      version_file = File.open('/opt/gitlab/embedded/service/gitlab-rails/VERSION')

      return unless version_file

      version = version_file.read.chomp
      puts "GitLab #{version} detected"
      version
    rescue
      puts "Unable to retrieve GitLab version"
    end
  end

  def load_config_yaml(config_file)
    config = YAML.load_file(config_file)

    @static_domain = config['storage']['static_domain']
    @bucket_name = config['storage']['bucket_name']
    @database_url = config['database']['url']
    @profiler_config = config['profiler']
    @auth_config = config['auth']
  end

  def username(auth_type)
    auth_config[auth_type]
  end

  def run_profile(url, auth_type)
    sql_filename, html_filename = output_files
    start_time = Time.now
    puts "Running #{url}, auth_type = #{auth_type}"
    base_cmd = "gitlab-rails r " + (use_bundled_script? ? bundled_script : DEPRECATED_PROFILE_SCRIPT)
    username_args = auth_type != 'anonymous' ? "-u #{username(auth_type)}" : ''
    cmd = "sudo #{base_cmd} '#{url}' #{username_args} -o #{html_filename} -s #{sql_filename}"
    puts cmd
    system(cmd)

    json_data = {
      url: url,
      auth_type: auth_type,
      test_group: base_directory,
      html_url: make_url(html_filename),
      sql_url: make_url(sql_filename),
      html_filename: html_filename,
      sql_filename: sql_filename,
      started_at: start_time,
      ended_at: Time.now
    }

    timings = parse_timings(sql_filename)
    json_data.merge!(timings) if timings

    unless timings[:total_ms].to_i > 0
      api_timings = parse_api_timings(sql_filename)
      json_data.merge!(api_timings) if api_timings
    end

    json_data
  end

  def upload_files(data)
    filenames = [data[:html_filename], data[:sql_filename]]

    filenames.each do |filename|
      zipped_filename = gzip_data(filename)

      if zipped_filename
        upload_to_s3(zipped_filename)
        puts "Deleting #{zipped_filename}"
        File.delete(zipped_filename)
      end

      File.delete(filename) if File.exist?(filename)
    end
  end

  def upload_to_s3(filename)
    basename = File.basename(filename)
    cmd = "aws s3 cp #{filename} s3://#{bucket_name}/#{base_directory}/#{basename}"
    cmd += " --content-encoding gzip"

    content_type_value = content_type(filename)
    cmd += " --content-type=\"#{content_type_value}\"" if content_type_value
    `#{cmd}`
  end

  def content_type(filename)
    return 'text/plain; charset=utf-8' if filename.end_with?('.txt.gz')
  end

  def chown_data(filename)
    # gitlab-rails runs as user git
    system("sudo chown $USER #{filename}")
  end

  def gzip_data(filename)
    chown_data(filename)
    cmd = "gzip #{filename}"
    puts cmd
    `#{cmd}`

    if $CHILD_STATUS.exitstatus.zero?
      filename + '.gz'
    else
      puts "gzip #{filename} failed!"
      exit 1
    end
  end

  def update_db(perf_data)
    db_updater.update_db(perf_data)
  end

  def make_url(filename)
    basename = File.basename(filename)
    URI.join(static_domain, File.join(base_directory, basename + '.gz')).to_s
  end

  def today_date
    Time.now.strftime('%Y%m%d')
  end

  def output_files
    uuid = SecureRandom.uuid
    html_filename = File.join('/tmp', uuid + '.html')
    sql_filename = File.join('/tmp', uuid + '.txt')

    [sql_filename, html_filename]
  end

  def parse_api_timings(sql_filename)
    # We parse Grape API timings in the format
    #
    # {"time":"2018-03-21T19:37:09.931Z","severity":"INFO","duration":49.21,"db":0.0,"view":49.21,"status":403,"method":"GET","path":"/api/v4/users","params":{},"host":"www.example.com","ip":"127.0.0.1","ua":null}
    json_lines = `grep ^{ #{sql_filename}`.split("\n")
    results = {}

    json_lines.reverse.each do |line|
      begin
        results = JSON.parse(line)
      rescue JSON::ParseError
        next
      end

      next unless %w[status view db].all? { |key| results.key?(key) }

      return {
        status_code: results['status'],
        view_time_ms: results['view'],
        sql_time_ms: results['db'],
        total_ms: results['view'] + results['db']
      }
    end

    {}
  end

  def parse_timings(sql_filename)
    # We parse the output from:
    # https://github.com/rails/rails/blob/master/actionpack/lib/action_controller/log_subscriber.rb#L27-L29
    #
    # For example:
    #
    # Completed 200 OK in 22194ms (Views: 3726.9ms | ActiveRecord: 17911.9ms)
    #
    # Also could be solved with a custom LogSubscriber
    last_lines = `grep "INFO.*ActiveRecord" #{sql_filename}`.split("\n")

    unless $CHILD_STATUS.exitstatus.zero?
      puts "Not able to tail file in #{sql_filename}"
      return {}
    end

    # Prefer the last line in case there are multiple queries
    last_lines.reverse.each do |line|
      results = parse_results_line(line)

      return results if results
    end

    {}
  end

  def parse_results_line(line)
    regexp = /INFO.*Completed (.*) (.*) in (.*)ms \((.*)\)/
    match_data = regexp.match(line)

    return unless match_data

    status_code = match_data[1].to_i
    status_description = match_data[2]
    total_ms = match_data[3].to_f
    details = match_data[4]
    results = { status_code: status_code,
                status_description: status_description,
                total_ms: total_ms }

    details_regexp = /.*Views: (.*)ms.*ActiveRecord:(.*)ms/
    details_match = details_regexp.match(details)

    if details_match
      view_time_ms = details_match[1].to_f
      sql_time_ms = details_match[2].to_f

      puts details_match
      puts "Processed view time: #{view_time_ms}, SQL time: #{sql_time_ms}"
      results[:view_time_ms] = view_time_ms
      results[:sql_time_ms] = sql_time_ms
    end

    puts results
    results
  end
end

if $PROGRAM_NAME == __FILE__
  if ARGV.length != 1
    puts "Usage: #{__FILE__} [YAML config file]"
    exit 1
  end

  profiler = GitLabProfiler.new(ARGV[0])
  puts profiler.profile
end
